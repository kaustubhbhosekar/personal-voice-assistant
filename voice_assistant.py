'''
Description:-

    Date:-13th April 2020.
    Topic:-Personal Voice Assistant.
    Functionality:-To built a system to replicate the functionality of a desktop voice assistant which would perform tasks as per commanded.
    Author:-Kaustubh.S.Bhosekar.
  
Further Scope:-
            This project depicts the general functionality of the desktop voice assistant. It can be further scaled up to be made as a full fledged voice assistant system to be implemented in various aspects of tecnological advencements.

Modules to be installed:-
    1. Install [ pyttsx3 ] for audio and voice related tasks use [pip install pyttsx3].
    2. Install [ pywin32 ] for running this application on Windows platform [ pip install pywin32 ]
    3. Install [ speechRecognition ] for recognition of user voice as an input [ pip install speechRecognition ]
    4. Install [ wikipedia ] for running wikipedia searches by using voice assistant [ pip install wikipedia ]

Refrences:-
    1 sapi5 :-Speach API by Microsoft
'''

#code

import pyttsx3
import datetime
import speech_recognition as sr
import wikipedia
import webbrowser
import os
import random
import smtplib

engine = pyttsx3.init('sapi5')
voices = engine.getProperty('voices')
engine.setProperty('voice',voices[0].id)

def speak(audio):
    '''
        This functionalty reads out the audio input provided by the user on the system's speakers.
    '''
    engine.say(audio)
    engine.runAndWait()

def greetTheUser():
    '''
        This functionalty greets the user based on the time of the day.
    '''
    hour = int(datetime.datetime.now().hour)
    if(hour >= 0 and hour < 12):
        speak('Good Morning!!')
    elif(hour >= 12 and hour < 18):
        speak('Good Afternoon!!')
    else:
        speak('Good Evening!!')

    speak('How May I Help you?')

def acceptCommandFromUser():
    '''
        This functionalty accepts voice command from the user as an input and returns appropriate output.
    '''
    r = sr.Recognizer()
    with sr.Microphone() as source:
        print('Listening...')
        r.pause_threshold = 1
        audio = r.listen(source)
        try:
            print('Recognizing...')
            query = r.recognize_google(audio, language='en-in')
            print(f'user said: {query}')
        except Exception as e:
            print(e)
            print("Say that again!!")
            return "None"

    return query

def sendEmail(to, content):
    '''
        This function sends an email using the configured email id.
        the email id and password can be further configured by fetching the fields from a file or a database.
    '''
    try:
        server = smtplib.SMTP('smtp.gmail.com', 587)
        server.ehlo()
        server.starttls()
        #configure the email and password in the below field 
        server.login('youremail@gmail.com','your-password-here')
        speak('Sending Email....')
        server.sendmail('youremail@gmail.com', to, content)
        speak('Email Sent....')
    except Exception as e:
        speak('Unable to send Email Kindly try again')


def main():
    greetTheUser()
    if(1):
        query = acceptCommandFromUser().lower()
        #Logic for executing tasks on the basis of query

        if 'wikipedia' in query:
            speak('Searching in Wikipedia....')
            content_to_search = query.replace('wikipedia','')
            results = wikipedia.summary(content_to_search, sentences=2)
            speak('According to Wikipedia..')
            print(results)
            speak(results)

        elif 'open youtube' in query:
            speak('Opening Youtube....')
            webbrowser.open('youtube.com')
        
        elif 'open google' in query:
            speak('Opening Google....')
            webbrowser.open('youtube.com')
        
        elif 'open stackoverflow' in query:
            speak('Opening Stackoverflow....')
            webbrowser.open('stackoverflow.com')

        elif 'play music' in query:
            speak('Playing Music....')
            music_dir = 'D:\\PYTHON\\AI_desktop_Assistant\\music'
            songs = os.listdir(music_dir)
            file_count = 0
            for path in os.listdir(music_dir):
                if os.path.isfile(os.path.join(music_dir, path)):
                    file_count += 1
            os.startfile(os.path.join(music_dir, songs[random.randint(0,file_count-1)]))

        elif 'the time' in query:
            strTime = datetime.datetime.now().strftime('%H:%M:%S')
            speak(f'Sir the time is {strTime}')

        elif 'open code' in query:
            speak('Opening Code....')
            code_path ='C:\\Users\\ADMIN\AppData\\Local\\Programs\\Microsoft VS Code\\Code.exe'
            os.startfile(code_path)

        elif 'open notepad++' in query:
            speak('Opening Notepad ++....')
            code_path ='C:\\Program Files\\Notepad++\\notepad++.exe'
            os.startfile(code_path)

        elif 'send and email' in query:
            try:
                speak('what should i mention in the mail?')
                content = acceptCommandFromUser()
                to = 'kaustubh.bhosekar@gmail.com'
                sendEmail(to, content)
                speak('Email has been sent!!')
            except Exception as e:
                print(e)
                speak("not able to send Email Kindly try again")

if __name__ == "__main__":
    main()